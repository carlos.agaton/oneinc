import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  baseUrl = 'http://oneinc.test/api';

  constructor(private http: HttpClient) { }

  getQuery(url: string){
    return this.http.get(this.baseUrl + url);
  }

  getPhotos(){
    return this.getQuery('photos?_limit=6');
  }

  getProducts(){
    return this.getQuery('/products');
  }

  getProduct(id: number){
    return this.getQuery('/products/' + id)
  }

  buyProduct(product: any){

    const payment = {
      'product_id': product.id,
      'amount': product.price,
      'user_id': 1,
      'currency': 'USD'
    }

    return this.http.post(this.baseUrl + '/payments', payment);
  }


  /**
   * PAYMENTS
   */

   getPayments(){
     return this.getQuery('/payments/1');
   }

   getPayment(id: number){
     return this.getQuery('/payments/' + id + '/edit');
   }

   updatePayment(id: number, changes: any){
     return this.http.put(this.baseUrl + '/payments/' + id, changes);
   }

   deletePayment(id: number){
     return this.http.delete(this.baseUrl + '/payments/' + id);
   }


}
