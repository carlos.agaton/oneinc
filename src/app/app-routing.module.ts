import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/components/home/home.component';
// import { PhotoComponent } from './pages/components/photo/photo.component';
import { DetailComponent } from './pages/components/detail/detail.component';
import { PaymentsComponent } from './pages/components/payments/payments.component';
import { EditPaymentComponent } from './pages/components/edit-payment/edit-payment.component';

const ROUTES: Routes = [
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'products/:id',
    component: DetailComponent
  },
  {
    path: 'payments',
    component: PaymentsComponent
  },
  {
    path: 'payments/:id',
    component: EditPaymentComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home',
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forRoot(ROUTES)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
