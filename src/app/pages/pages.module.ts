import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { PhotoComponent } from './components/photo/photo.component';
import { DetailComponent } from './components/detail/detail.component';
import { BaseUrlPipe } from '../pipes/base-url.pipe';
import { RouterModule } from '@angular/router';
import { PaymentsComponent } from './components/payments/payments.component';
import { EditPaymentComponent } from './components/edit-payment/edit-payment.component';


import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [HomeComponent, PhotoComponent, DetailComponent, BaseUrlPipe, PaymentsComponent, EditPaymentComponent],
  imports: [
    CommonModule, RouterModule, ReactiveFormsModule
  ],
  exports: [HomeComponent, DetailComponent, BaseUrlPipe, PaymentsComponent, EditPaymentComponent]
})
export class PagesModule { }
