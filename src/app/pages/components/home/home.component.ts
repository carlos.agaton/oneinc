import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../../../services/photos.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  products: any[] = [];

  constructor(private photosService: PhotosService) { }

  ngOnInit() {
    this.photosService.getProducts().subscribe(
      (data: any) => {
        console.log(data);
        this.products = data;
      }
    )
  }

}
