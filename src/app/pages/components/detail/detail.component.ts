import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PhotosService } from '../../../services/photos.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {

  constructor(
    private actRoute: ActivatedRoute, 
    private service: PhotosService,
    ) { }
  product :any

  ngOnInit() {

    this.actRoute.params.subscribe(
      params => {

        this.service.getProduct(params.id).subscribe(
          (product: any ) => {
            this.product= product;
          }
        )

      }
    )

  }

  buy(){
    console.log(this.product);
    this.service.buyProduct(this.product).subscribe(
      payment => {
        console.log(payment);
      }
    );
  }

}
