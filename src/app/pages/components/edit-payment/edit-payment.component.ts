import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PhotosService } from '../../../services/photos.service';

@Component({
  selector: 'app-edit-payment',
  templateUrl: './edit-payment.component.html',
  styleUrls: ['./edit-payment.component.css']
})
export class EditPaymentComponent implements OnInit {
  
  form: FormGroup;
  productId: any;

  constructor(
    private formBuilder: FormBuilder,
    private actRoute: ActivatedRoute,
    private service: PhotosService,
    private router: Router,
  ) {
    this.formBuild();
   }

  ngOnInit() {
    this.actRoute.params.subscribe(
      params => {
        this.productId = params.id;
        this.service.getPayment(this.productId).subscribe(
          product => {
            this.form.patchValue(product);
          }
        )
      }
    )
  }

  private formBuild() {
    this.form = this.formBuilder.group({
      currency: ['', [Validators.required]],
      amount: ['', [Validators.required]],
    })
  }

  savePayment(event: Event){
    event.preventDefault();

    if (this.form.valid) {
      const payment = this.form.value;

      // console.log(payment);

      this.service.updatePayment(this.productId, payment).subscribe(
        changes => {
          console.log(changes);
          this.router.navigate(['/payments']);
        }
      )
    }

  }

}
