import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../../../services/photos.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent implements OnInit {

  payments: any[];

  constructor(
    private service: PhotosService
  ) { }

  ngOnInit() {
    this.service.getPayments().subscribe(
      (payments: any) => {
        this.payments = payments
        console.log(payments);
      }
    )
  }

  delete(id: number, idx: number){
    this.service.deletePayment(id).subscribe(
      response => {
          this.payments.splice(idx, 1)
      }
    )
  }

}
